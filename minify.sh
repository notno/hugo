#!/usr/bin/env bash

cd "$1"

shopt -s globstar

echo "[*] Now minifying all .html files..."

for filename in **/*.html; do
    [ -e "$filename" ] || continue
    echo $filename
    html-minifier --minify-js --minify-css --collapse-whitespace --remove-comments --remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes --remove-tag-whitespace --use-short-doctype "$filename" -o "$filename"
done

echo "[*] Now minifying all .css files..."

for filename in **/*.css; do
    [ -e "$filename" ] || continue
    echo $filename
    postcss "$filename" --replace
done

echo "[*] Now minifying all .js files..."

for filename in **/*.js; do
    [ -e "$filename" ] || continue
    echo $filename
    uglifyjs --compress --mangle -o "$filename" -- "$filename"
done

echo "[+] All done."