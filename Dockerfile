FROM alpine:edge

RUN apk --no-cache add hugo git gzip npm bash vips-tools

# https://stackoverflow.com/questions/57534295/npm-err-tracker-idealtree-already-exists-while-creating-the-docker-image-for/65443098
WORKDIR /stuff

RUN npm install html-minifier postcss postcss-cli uglify-js autoprefixer -g && npm install cssnano --save-dev

COPY minify.sh postcss.config.js .
